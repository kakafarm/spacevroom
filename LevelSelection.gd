extends Control


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var item_list: ItemList = $VBoxContainer/ItemList


const item_text_to_level_path: Dictionary = {
	"Vanilla Sky": "res://Level1.tscn"
}


var level_selected: String = ""


func _ready() -> void:
	pass


func _on_ItemList_item_selected(index: int) -> void:
	var item_text: String = item_list.get_item_text(index)
	var level_path: String = item_text_to_level_path[item_text]
	print_debug(index, " ", item_text, " ", level_path)
	get_tree().change_scene(level_path)


func _on_Button_pressed() -> void:
	get_tree().change_scene("res://Level1.tscn")
