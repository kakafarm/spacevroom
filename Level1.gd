extends Spatial


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var boxes := $Boxes


func _ready() -> void:
	var level_text_file: File = File.new()
	var file_open_error := level_text_file.open("res://level1.txt", File.READ)
	if file_open_error:
		push_error("Level open error: %s" % file_open_error)
	var level_text := level_text_file.get_as_text()
	level_text_file.close()
	var loaded_boxes := LevelLoader.load_level(level_text)
	boxes.add_child(loaded_boxes)


func _input(event: InputEvent) -> void:
	if event.is_action("quit_level"):
		get_tree().change_scene("res://LevelSelection.tscn")
	if event.is_action("reset_level"):
		get_tree().reload_current_scene()


func _on_Player_off_the_deep_end() -> void:
	get_tree().reload_current_scene()


func _on_Player_reached_goal() -> void:
	get_tree().change_scene("res://LevelSelection.tscn")
