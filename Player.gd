extends KinematicBody


# Space Vroom, an unoriginal space racer.
# Copyright (C) 2021  Yuval Langer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


onready var ray_cast1: RayCast = $RayCast1
onready var ray_cast2: RayCast = $RayCast2
onready var ray_cast3: RayCast = $RayCast3
onready var ray_cast4: RayCast = $RayCast4


var velocity: Vector3 = Vector3.ZERO
var driving_speed: float = 0
var direction: int = 0
var last_pressed_direction: String = ""
var on_floor_flag: bool = false


signal speed_changed
signal off_the_deep_end
signal reached_goal


onready var forward_speed_change_timer: Timer = $ForwardSpeedChangeTimer


func _ready() -> void:
	print_debug(forward_speed_change_timer.time_left)


func is_near_floor(ray_cast: RayCast) -> bool:
	if ray_cast.is_colliding():
		var origin := ray_cast.global_transform.origin
		var collision_point := ray_cast.get_collision_point()
		var distance := origin.distance_to(collision_point)
		return distance <= Constants.on_floor_maximum_distance
	return false


func is_near_goal(ray_cast: RayCast) -> bool:
	var collider := ray_cast.get_collider()
	if collider:
		return collider.is_in_group("goal")
	return false


func _physics_process(delta: float) -> void:
	var near_floor := (is_near_floor(ray_cast1)
		or is_near_floor(ray_cast2)
		or is_near_floor(ray_cast3)
		or is_near_floor(ray_cast4))
	var near_goal := (is_near_goal(ray_cast1)
		or is_near_goal(ray_cast2)
		or is_near_goal(ray_cast3)
		or is_near_goal(ray_cast4))

	if near_goal and near_floor:
		emit_signal("reached_goal")
	if near_floor:
		direction = 0
		if (
			(not Input.is_action_just_pressed("ui_right")) or
			(not Input.is_action_just_pressed("ui_left"))
			):
			if Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_left"):
				if last_pressed_direction == "right":
					direction = 1
				elif last_pressed_direction == "left":
					direction = -1
			else:
				if Input.is_action_pressed("ui_right"):
					direction = 1
				if Input.is_action_pressed("ui_left"):
					direction = -1
		if Input.is_action_just_pressed("ui_right"):
			last_pressed_direction = "right"
			direction = 1
		if Input.is_action_just_pressed("ui_left"):
			last_pressed_direction = "left"
			direction = -1
		if Input.is_action_just_released("ui_right"):
			if Input.is_action_pressed("ui_left"):
				last_pressed_direction = "left"
				direction = -1
			else:
				last_pressed_direction = ""
				direction = 0
		if Input.is_action_just_released("ui_left"):
			if Input.is_action_pressed("ui_right"):
				last_pressed_direction = "right"
				direction = 1
			else:
				last_pressed_direction = ""
				direction = 0
	if Input.is_action_pressed("ui_up") and forward_speed_change_timer.time_left == 0.0:
		print_debug(forward_speed_change_timer.time_left)
		forward_speed_change_timer.start()
		driving_speed += Constants.forward_speed_increment
		driving_speed = clamp(driving_speed, 0, Constants.maximum_forward_speed)
		emit_signal("speed_changed", driving_speed)
	if Input.is_action_pressed("ui_down") and forward_speed_change_timer.time_left == 0.0:
		print_debug(forward_speed_change_timer.time_left)
		forward_speed_change_timer.start()
		driving_speed -= Constants.forward_speed_increment
		driving_speed = clamp(driving_speed, 0, Constants.maximum_forward_speed)
		emit_signal("speed_changed", driving_speed)


	velocity.x = direction * Constants.side_speed
	velocity.z = -driving_speed

	if near_floor and Input.is_action_just_pressed("jump"):
		velocity.y += Constants.jump_impulse
	if near_floor:
		velocity.y -= Constants.on_the_floor_fall_acceleration * delta
	else:
		velocity.y -= Constants.fall_acceleration * delta

	velocity = move_and_slide(velocity, Vector3.UP)

	if translation.y < -5:
		emit_signal("off_the_deep_end")
