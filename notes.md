## Display background image by:

1. Add MeshInstance to camera node.
2. Set MeshInstance's Mesh property to PlaneMesh.
3. Load image file:
    MeshInstance -> Mesh -> PlaneMesh -> Material -> Albedo -> Load (in the popup menu).
4. Change rotation of the MeshInstance to be directly in front of the Camera.
5. There is `display_background()` code in the Camera.gd script to set the distance.
6. Change Camera's "far", most distant rendered thing, property to fit in the MeshInstance.
